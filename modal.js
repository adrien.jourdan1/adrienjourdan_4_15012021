function editNav() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

// DOM Elements
const modalbg = document.querySelector(".bground");
const modalBtn = document.querySelectorAll(".modal-btn");
const formData = document.querySelectorAll(".formData");
const closeBtn = document.querySelectorAll(".close");
const submitBtn = document.getElementById("btn-submit");

const modalForm = document.querySelector("#modal-form");

let firstName = modalForm.elements["first"];
let lastName = modalForm.elements["last"];
let email = modalForm.elements["email"];
let birthDate = modalForm.elements["birthdate"];
let quantity = modalForm.elements["quantity"];
let locations = document.getElementsByName("location");
let chosenLocation;
let genConditions = document.getElementById("checkbox1");

let inputList = [
  firstName,
  lastName,
  email,
  birthDate,
  quantity,
  locations[0],
  genConditions
];

// launch modal event
modalBtn.forEach((btn) => btn.addEventListener("click", launchModal));
// launch modal form
function launchModal() {
  modalbg.style.display = "block";
}

// close modal event
closeBtn.forEach((btn) => btn.addEventListener("click", closeModal));
// close modal form
function closeModal() {
  modalbg.style.display = "none";
}

// Form Validation
const formValidation = () => {

  //Regex 
  let nameRegex = /^\D{2,}$/;
  let emailRegex = /^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]­{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$/;
  let quantityRegex = /^\d{1,2}$/;
  let birthDateRegex = /\d/;

  // Locations' checkboxes validation -- Watch each checkbox if one is checked
  let locationIsValid = false;
  for(let i=0; i < locations.length; i++){
    if(locations[i].checked){                  
      locationIsValid = true;
      chosenLocation = locations[i].value;
      break; //get out of the loop 
    }
  }

  // Error function (closest() looks for the closest parent)
  const error = (elt, errorMsg) => {
    elt.closest(".formData").setAttribute("data-error-visible", "true");
    elt.closest(".formData").setAttribute("data-error", errorMsg);
  }

  // Error msg invisible set/reset
  const errorAbort = (elt) => {
    elt.closest(".formData").removeAttribute("data-error");
    elt.closest(".formData").setAttribute("data-error-visible", "false");
  }

  // errorAbort function on each formData before Form Validation
  inputList.forEach(elt => errorAbort(elt));

  // Form unvalidation conditions
  let errors = 0;

  if(nameRegex.exec(firstName.value) == null){ //cf doc exec()
    error(firstName, "Veuillez saisir un prénom valide (2 caractères min.)");
    errors++;
  }
  if(nameRegex.exec(lastName.value) == null){
    error(lastName, "Veuillez saisir un nom valide (2 caractères min.)");
    errors++;
  }
  if(emailRegex.exec(email.value) == null){
    error(email, "Veuillez saisir une adresse mail valide, exemple : john.doe@rock.fr");
    errors++;
  }
  if(birthDateRegex.exec(birthDate.value) == null){
    error(birthDate, "Veuillez saisir une date valide, exemple : 26/03/1986");
    errors++;
  }
  if(quantityRegex.exec(quantity.value) == null){
    error(quantity, "Veuillez saisir un nombre entre 0 et 99");
    errors++;
  }
  if(!locationIsValid){
    error(locations[0], "Veuillez sélectionner une ville");
    errors++;
  }
  if(genConditions.checked == false){
    error(genConditions, "Veuillez accepter les conditions générales d'utilisation");
    errors++;
  }

  //function return true OR false if it finds any error 
  if(errors > 0){
    return false;
  }else{
    return true;
  }
}


//Once validation and submission are ok, lauch a success message into the modal
const modalSuccess = () => {
  const successCloseBtn = document.getElementById("btn-close");
  const successMsg = document.getElementById("msg-success");
  modalForm.style.display = "none";
  successCloseBtn.style.display = "block";
  successMsg.style.display = "block";
  successCloseBtn.addEventListener("click", closeModal);
}

//Submit function, create a new form to collect data from html form fields
const submitForm = () => {
  //Get form data
  let formData = new FormData();
  formData.append("first name", firstName.value);
  formData.append("last name", lastName.value);
  formData.append("email", email.value);
  formData.append("birthdate", birthdate.value);
  formData.append("chosenLocation", chosenLocation);

  //AJAX
  let request = new XMLHttpRequest();
  request.open("POST", "https://mockbin.com/request");
  request.send(formData);
  
  request.onreadystatechange = function() {
    if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
      modalSuccess();
    }
  };
}


// On submit, launch form validation and abort submit if something return false
submitBtn.addEventListener("click", function(event){
  let isFormValid = formValidation();
  if(isFormValid){
    submitForm();
  }
});
